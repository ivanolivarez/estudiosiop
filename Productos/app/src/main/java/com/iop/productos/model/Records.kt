package com.iop.productos.model

import java.io.Serializable


data class Records(
    var productDisplayName: String = "",
    var listPrice: String = "",
    var category: String = "",
    var smImage: String = ""
):Serializable