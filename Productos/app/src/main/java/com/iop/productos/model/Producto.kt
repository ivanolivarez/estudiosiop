package com.iop.productos.model

import java.io.Serializable


class Producto : Serializable {
    var titulo: String = ""
    var precio: String = ""
    var ubicacion: String = ""
    var imagen: String = ""
}