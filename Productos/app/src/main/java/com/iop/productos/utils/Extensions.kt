package com.iop.productos.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun ViewGroup.inflate(idLayout : Int): View = LayoutInflater.from(this.context).inflate(idLayout,this,false)