package com.iop.productos

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.iop.productos.model.Records
import com.iop.productos.utils.inflate
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_producto.view.*

class ProductosAdapter: RecyclerView.Adapter<ProductosAdapter.ViewHolder> () {

    private var listaProductos: MutableList<Records> = ArrayList()

    fun productosAdapter(listaProductos: MutableList<Records>) {
        this.listaProductos = listaProductos
    }

    fun agregarElementos(items : MutableList<Records>){
        listaProductos.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItemCount() = listaProductos.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.card_producto))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(listaProductos[position])

    inner class ViewHolder(private val card: View): RecyclerView.ViewHolder(card){
        fun bind(producto: Records) = with(card) {
            textViewTitle.text = producto.productDisplayName
            textViewPrice.text = ("$ "+ producto.listPrice)
            textViewLocation.text = producto.category

            Picasso.get()
                .load(producto.smImage)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .error(R.drawable.ic_launcher_background)
                .into(networkImageProduct)

        }
    }
}