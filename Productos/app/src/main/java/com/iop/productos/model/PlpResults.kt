package com.iop.productos.model

import java.io.Serializable

data class PlpResults(
    var label:String = "",
    var records: MutableList<Records> = ArrayList()
): Serializable