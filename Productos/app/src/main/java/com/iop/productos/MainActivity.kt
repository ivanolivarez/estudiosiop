package com.iop.productos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SearchView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.JsonRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.iop.productos.constantes.Constantes
import com.iop.productos.model.Objeto
import com.iop.productos.model.Records
import kotlinx.android.synthetic.main.activity_main.*
import java.net.URLEncoder

class MainActivity : AppCompatActivity() {

    private val mRequestTag = ""
    private var emRequest: RequestQueue? = null
    private val productAdapter: ProductosAdapter = ProductosAdapter()
    private var productList: MutableList<Records> = ArrayList()
    private var stringBusqueda: String? = ""
    private var items = 20
    private var pagina = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        emRequest = Volley.newRequestQueue(this)

        recyclerViewProducts.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText!!.isEmpty()) {
                    stringBusqueda = ""
                }
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                stringBusqueda = query
                pagina = 1
                buscarProductos(stringBusqueda)
                return false
            }
        })

        swipeRefresh.setOnRefreshListener {
            pagina = 1
            buscarProductos(stringBusqueda)
        }

        swipeRefresh.setOnLoadmoreListener {
            pagina++
            buscarProductos(stringBusqueda)
        }

    }

    override fun onResume() {
        super.onResume()
        if (emRequest != null) {
            emRequest!!.cancelAll(mRequestTag)
        }
        if (!stringBusqueda.isNullOrEmpty()) {
            productList.clear()
            productAdapter.notifyDataSetChanged()
            showProgress(true)
            obtenerLista(stringBusqueda!!)
        }
    }

    override fun onStop() {
        super.onStop()
        if (emRequest != null) {
            emRequest!!.cancelAll(mRequestTag)
        }
    }

    fun buscarProductos(query: String?){
        obtenerLista(query!!)
        showProgress(true)
    }

    fun iniciarLista(lista: MutableList<Records>){
        if (pagina == 1){
            productList.clear()
            productList.addAll(lista)
            recyclerViewProducts.visibility = View.VISIBLE
            productAdapter.productosAdapter(productList)
            recyclerViewProducts.adapter = productAdapter
            recyclerViewProducts.adapter!!.notifyDataSetChanged()
        }else {
            (recyclerViewProducts.adapter!! as ProductosAdapter).agregarElementos(lista)
        }
    }

    private fun showProgress(show: Boolean) {
        progressBars!!.visibility = if (show) View.VISIBLE else View.GONE
    }

    fun obtenerLista(palabra: String) {
        val gson = Gson()
        val busqueda =  URLEncoder.encode(palabra,"utf-8")
        val url = "https://shoppapp.liverpool.com.mx/appclienteservices/services/v3/plp?force-plp=true&search-string=$busqueda&page-number=$pagina&number-of-items-per-page=$items"
            val jsonObject = JsonObjectRequest(Request.Method.GET, url, null, { response ->
                swipeRefresh.finishRefresh()
                swipeRefresh.finishLoadmore()
                showProgress(false)
                val objeto = gson.fromJson(response.toString(), Objeto::class.java)
                if (objeto.plpResults!!.records.isNotEmpty()){
                    val lista = objeto.plpResults!!.records
                    iniciarLista(lista)
                }

        }, { error ->
                showProgress(false)
                swipeRefresh.finishRefresh()
                swipeRefresh.finishLoadmore()
            when (error) {
                is ServerError -> Toast.makeText(this, getString(R.string.error_server), Toast.LENGTH_SHORT).show()
                is TimeoutError -> Toast.makeText(this, getString(R.string.error_timeout), Toast.LENGTH_SHORT).show()
                is NoConnectionError -> Toast.makeText(this, getString(R.string.error_no_connection), Toast.LENGTH_SHORT).show()
                is NetworkError -> Toast.makeText(this, getString(R.string.error_network), Toast.LENGTH_SHORT).show()
                is AuthFailureError -> Toast.makeText(this, getString(R.string.error_auth_failure), Toast.LENGTH_SHORT).show()
                else -> {
                    Toast.makeText(this, getString(R.string.error_acceso), Toast.LENGTH_SHORT)
                        .show()
                }

            }

        })
        jsonObject.tag = mRequestTag
        emRequest!!.add(jsonObject).retryPolicy= DefaultRetryPolicy(Constantes.TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
    }
}
